import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { lastValueFrom } from 'rxjs';
import { LodingService, SweetalertDialogService, position } from 'src/share/center/export-cneter';
import { AuthenService } from '../authen.service';
import { LocalStorageService } from 'src/share/local-storage/local-storage.service';
import { pathURL } from 'src/share/config-server/path-fron-end';


export class Login{
  username:any="";
  password:any="";
}
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent {
  constructor(
    private route: Router,
    private sweetAlert:SweetalertDialogService,
    private authenService: AuthenService,
    private loading: LodingService,
    private localStorageService:LocalStorageService,

  ){
  }
  login: Login = new Login();
  showPass: boolean = false;
  isLoading:boolean = false;
  role:any = "";

  async ngOnInit() {
    this.getQueryParame();
  }

  getQueryParame(){
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const teken = urlParams.get('token');
    const expire = urlParams.get('expire');
    console.log(new Date(expire!));
    if(teken && expire){
      this.localStorageService.saveData("token",teken);
      this.localStorageService.saveData("expire",expire);
      // this.route.navigate([`/${pathURL.Apply}`])
    }
    this.route.navigate([`/${pathURL.Register}`])
  }

  async onLogin(){
    if(this.login.password != "" && this.login.username != ""){
      this.isLoading = true;
      const startTime = Date.now();
      console.log(this.login);
      await lastValueFrom(this.authenService.serviceLogin(this.login))
      .then(async (json:any)=>{
        console.log(json);
        this.isLoading = await this.loading.isLoadingFalse(startTime);;
        if(json.code == 200){
          this.localStorageService.saveData("token",json.token);
          this.localStorageService.saveData("expire",json.expire);
          this.sweetAlert.showSuccess(json.message,position.center,2000)
          setTimeout(() => {
            this.route.navigate([`/${pathURL.Register}`])
          }, 2000);
        }
        else{
          this.sweetAlert.showError(json.message,position.center)
        }
      })
      .catch((err)=>{
          this.isLoading = false;
          this.sweetAlert.showWarning("Please enter your information.",position.center)
          console.error("Login error: ", err);
      })
    }
    else{
      this.sweetAlert.showWarning("Please enter your information.",position.center)
    }
  }


}
