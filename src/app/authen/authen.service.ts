import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigServerBackEnd } from 'src/share/config-server/path-back-end';

@Injectable({
  providedIn: 'root'
})
export class AuthenService {

  constructor(
    private http: HttpClient,
    private config:ConfigServerBackEnd
  ) 
  { }

  serviceLogin(dataLogin:any){
    const url = `${this.config.url}/auth/login`;
    return this.http.post(url,JSON.stringify(dataLogin))
  }
}
