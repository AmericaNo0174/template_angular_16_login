import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { AuthenRoutingModule } from './authen.routing.module';
import { AppModule } from '../app.module';
import { CenterModule } from 'src/share/center/center.module';



@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    AuthenRoutingModule,
    CenterModule
  ]
})
export class AuthenModule { }
