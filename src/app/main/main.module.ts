import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CenterModule } from 'src/share/center/center.module';
import { MainRoutingModule } from './main-routing.module';



@NgModule({
  declarations: [
 
  ],
  imports: [
    CommonModule,
    CenterModule,
    MainRoutingModule
  ],
})
export class MainModule { }
