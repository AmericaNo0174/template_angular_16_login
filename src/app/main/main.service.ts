import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigServerBackEnd } from 'src/share/config-server/path-back-end';

@Injectable({
  providedIn: 'root'
})
export class MainService {
  constructor(
    private http: HttpClient,
    private config:ConfigServerBackEnd
  ) 
  { }

  serviceGetListDD(catType: any) {
    const url = `${this.config.url}/center/list-dropdown`;
    let queryParams = { "catType": catType };
    return this.http.get(url, { params: queryParams })
  }

}
