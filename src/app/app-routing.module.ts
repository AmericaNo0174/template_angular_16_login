import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { pathURL } from 'src/share/config-server/path-fron-end';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./authen/authen.module').then(x => x.AuthenModule),
    // canActivate:[UnauthenticationGuard]
  },
  {
    path:pathURL.Register,
    loadChildren: () => import('./main/main.module').then(x => x.MainModule),
  }
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
