export const environment = {

    production: true,
    name:'development environment',
    url:(window as { [env: string]: any })["env"]["OA_GATEWAY_ENDPOINT"] || 'https://test-oa-gateway.ask.co.th',
};

