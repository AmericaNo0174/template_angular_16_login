import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';
import { SelectButtonModule } from 'primeng/selectbutton';
import { FormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { TreeModule } from 'primeng/tree';
@NgModule({
  declarations: [
    LoadingSpinnerComponent,
  ],
  imports: [
    CommonModule,
    SelectButtonModule,
    FormsModule,
    ButtonModule,
    TreeModule
  ],
  exports: [
    LoadingSpinnerComponent,
    SelectButtonModule,
    FormsModule,
    ButtonModule,
    TreeModule
    
  ]
})
export class CenterModule { }
