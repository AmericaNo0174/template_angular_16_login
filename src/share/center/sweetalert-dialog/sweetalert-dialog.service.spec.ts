import { TestBed } from '@angular/core/testing';

import { SweetalertDialogService } from './sweetalert-dialog.service';

describe('SweetalertDialogService', () => {
  let service: SweetalertDialogService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SweetalertDialogService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
