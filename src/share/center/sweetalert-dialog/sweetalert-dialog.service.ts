import { Injectable, HostListener } from '@angular/core';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class SweetalertDialogService {
  constructor() { }

  showError(message: any,position:any) {
    Swal.fire({
      title: 'Error!',
      position: position,
      text: message,
      icon: 'error',
      confirmButtonText: 'OK',
      customClass: {
        icon: 'custom-icon-class', // Define your custom CSS class here
      },
      confirmButtonColor: '#d9534f', // Change the confirm button color
    });
  }
  
  showWarning(message: any,position:any) {
    Swal.fire({
      title: 'Warning!',
      text: message,
      position: position,
      icon: 'warning',
      confirmButtonText: 'OK',
      customClass: {
        icon: 'custom-icon-class', // Define your custom CSS class here
      },
      confirmButtonColor: '#d9534f', // Change the confirm button color
    });
  }

  showSuccess(message: any,position:any,time:any){
    Swal.fire({
      position: position,
      icon: "success",
      title: message,
      showConfirmButton: false,
      timer: time
    });
  }
}

export const position = {
  top:'top',
  topStart:'top-start', 
  topEnd:'top-end', 
  center:'center', 
  centerStart:'center-start', 
  centerEnd:'center-end', 
  bottom:'bottom', 
  bottomStart:'bottom-start', 
  bottomEnd:'bottom-end',
}