import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LodingService {

  constructor() { }

  async isLoadingFalse(startTime: any){
    let elapsedTime = Date.now() - startTime;
    if (elapsedTime < 1000) {
      await new Promise(resolve => setTimeout(resolve, 500)); // รอให้ผ่านเวลา 500 มิลลิวินาที
    }
    return false;
  }
}
