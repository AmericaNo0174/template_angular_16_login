import { Route, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HTTP_INTERCEPTORS, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from "rxjs/operators";
import { LocalStorageService } from '../local-storage/local-storage.service';

@Injectable({
    providedIn: 'root',
})
export class HttpRequestInterceptor implements HttpInterceptor {
    private isRefreshing = false;

    constructor(
        private localStorageService: LocalStorageService,
        private router: Router
    ) {

    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const localToken = this.localStorageService.getData("token")
        const expire = this.localStorageService.getData("expire")
        const arrUrl = req.url.split("/");
        if (arrUrl[arrUrl.length - 1] == "login") {
            req = req.clone({
                setHeaders: {
                    // Authorization:'bearer'+localToken
                    'Content-Type': "application/json; charset=utf-8",
                },

            })
        }
        // else{
        //     if(localToken && expire){
        //         if (arrUrl[arrUrl.length - 1] != "insert-followup" && arrUrl[arrUrl.length - 1] != "follow-up"  && arrUrl[arrUrl.length - 1] != "actionIssueHead") { //กรณีต้องการส่งแบบ form-data ให้ผ่านไปได้เลย
        //             req = req.clone({
        //                 setHeaders: {
        //                     // Authorization:'bearer'+localToken
        //                     'Content-Type': "application/json; charset=utf-8",
        //                     "Authorization": 'Bearer ' + localToken
        //                 },
        
        //             })
        //         }
        //         else{
        //             req = req.clone({
        //                 setHeaders: {
        //                     "Authorization": 'Bearer ' + localToken
        //                 },
        
        //             })
        //         }
        //     }
        //     else{
        //         console.error("Please log in successfully!");
        //         this.router.navigate(['/']);
        //     }
        // }
        // console.log(req);
        // return next.handle(req);
        return next.handle(req).pipe(
        catchError((err: HttpErrorResponse) => {
           let errorMsg = '';
           if (err.error instanceof ErrorEvent) {
              console.log('This is client side error');
              errorMsg = `Error: ${err.error.message}`;
           } else {
              console.log('This is server side error');
              errorMsg = `Error Code: ${err.status},  Message: ${err.message}`;
              console.log("status error:",err.status);
              if(err.status == 401 ){
                console.log('Token is expire!');
                this.localStorageService.clearData();
                this.router.navigate(['/']);
              }
           }
           console.log(errorMsg);
           return throwError(err);
        })
          )
    }
}