import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  language = new Subject<string>();

  constructor() {

  }
  getLanguage(){
    return this.language.asObservable();
  }
  setLanguage(data:string){
    this.language.next(data) 
  }
  
  public saveData(key: string, value: string) {
    localStorage.setItem(key, value);
  }

  public getData(key: string) {
    return localStorage.getItem(key)
  }
  public removeData(key: string) {
    localStorage.removeItem(key);
  }

  public clearData() {
    localStorage.clear();
  }

  public getUserData() {
    const token = localStorage.getItem('token'); // ดึง JWT Token จาก localStorage
    if (!token) {
      return null; // ไม่มี Token ใน localStorage
    }
    const parts = token.split('.');
    if (parts.length !== 3) {
      return null; // Token ไม่ถูกต้อง
    }
    const payload = parts[1];
    try {
      const decodedPayload = JSON.parse(atob(payload));
      return decodedPayload;
    } catch (error) {
      console.error('Error decoding JWT payload:', error);
      return null; // เกิดข้อผิดพลาดในการถอดรหัส Payload
    }
  }
  
}
